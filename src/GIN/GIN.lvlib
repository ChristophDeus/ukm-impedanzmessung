﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">13714175</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16746748</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)P!!!*Q(C=\&gt;7^&lt;2N"%-8R2]/"5R:AQ#&lt;AU.'E#BGI!1+O9/!/G&amp;\)&amp;FY,&lt;)%N4!NMA3W=`\==#!IE-&lt;%%1&gt;#O^M2\NR]`\2V0UCD@J+_;&lt;Z7\&amp;]PD[UPHJ&gt;\.4V[@LY@2[?HL08Z_&gt;PS,]^^;`RH`D;,0]FE_&gt;JFO@AF?`8OQUP)CIB8.N+3&amp;FG8H)C`S)C`S)C^SEZP=Z#9XO=G40-G40-G40-G$0-C$0-C$0-B(*2?ZS%5/K6C]7+C9N*CA[!R&amp;R;(Q&amp;*\#5XAY6?%J0)7H]"1?OKDQ&amp;*\#5XA+$].5?!J0Y3E]B9?JBK2'*=&gt;4?*B?RG-]RG-]RM/3-BY$-)O:C=UE-'1OGA`'9TT'QU=:D`%9D`%9$Z&gt;F0-:D0-:D0!Q:O_+B73IZ(K:2YEE]C3@R*"[G6O**0)EH]31?FF0C34Q*)FEQG2S#EE&amp;*B_1E]31?@CHR**\%EXA3$Z@'(=KR-YNGK?2Y!E`A#4S"*`!QB1*0Y!E]A3@Q-+U#4_!*0)%H],#5!E`A#4Q"*&amp;C5Z26-&amp;AQ-/A6"Y/&amp;H0#UR\J+(*%;6_O&amp;60Z4KBUX^%+E@$P6.6^^-^5V3&lt;\Z[5^7&lt;J&gt;Y%^2_H2KMR[E85AZ?/OH!]UU[U)_V!W^.WN#VN1VMP1`^TR]PFIP0ZL.0JJ/0RK-0BI0V_L^VOJ_VWK]VGI`6[`@!;_%&amp;^?#&amp;=XUM4H[@P^[PJZ^`6^0P0;PJV@TUH@Y0`T_`AX;AP?LQ'?`10J8J12Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="send" Type="Folder">
		<Item Name="MSG_Import Cancel.vi" Type="VI" URL="../send/MSG_Import Cancel.vi"/>
		<Item Name="MSG_Import Selected Fit Data.vi" Type="VI" URL="../send/MSG_Import Selected Fit Data.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Actual Time Scalevalues.vi" Type="VI" URL="../support/Actual Time Scalevalues.vi"/>
		<Item Name="Autoscale Graph Import.vi" Type="VI" URL="../support/Autoscale Graph Import.vi"/>
		<Item Name="Autoscale Graph Interpolate.vi" Type="VI" URL="../support/Autoscale Graph Interpolate.vi"/>
		<Item Name="Autoscale Graph Normalize.vi" Type="VI" URL="../support/Autoscale Graph Normalize.vi"/>
		<Item Name="Autoscale Graph Statistic.vi" Type="VI" URL="../support/Autoscale Graph Statistic.vi"/>
		<Item Name="Calculate Mean Value.vi" Type="VI" URL="../support/Calculate Mean Value.vi"/>
		<Item Name="Calculate Scroll Position.vi" Type="VI" URL="../support/Calculate Scroll Position.vi"/>
		<Item Name="Determine Timepoints.vi" Type="VI" URL="../support/Determine Timepoints.vi"/>
		<Item Name="Export Data to TDMS.vi" Type="VI" URL="../support/Export Data to TDMS.vi"/>
		<Item Name="Extract ElectrodeConfig Strings.vi" Type="VI" URL="../support/Extract ElectrodeConfig Strings.vi"/>
		<Item Name="Extract ElectrodeConfig Values.vi" Type="VI" URL="../support/Extract ElectrodeConfig Values.vi"/>
		<Item Name="Format Graph-Import Data.vi" Type="VI" URL="../support/Format Graph-Import Data.vi"/>
		<Item Name="Format Graph-Normalize Data.vi" Type="VI" URL="../support/Format Graph-Normalize Data.vi"/>
		<Item Name="Format Graph-Interpolate Data.vi" Type="VI" URL="../support/Format Graph-Interpolate Data.vi"/>
		<Item Name="Format Graph-Statistic Data.vi" Type="VI" URL="../support/Format Graph-Statistic Data.vi"/>
		<Item Name="Get Interval Steps.vi" Type="VI" URL="../support/Get Interval Steps.vi"/>
		<Item Name="Interpolate Data.vi" Type="VI" URL="../support/Interpolate Data.vi"/>
		<Item Name="Normalize Data.vi" Type="VI" URL="../support/Normalize Data.vi"/>
		<Item Name="Select Electrode Listbox.vi" Type="VI" URL="../support/Select Electrode Listbox.vi"/>
		<Item Name="Set Listbox Symbols.vi" Type="VI" URL="../support/Set Listbox Symbols.vi"/>
		<Item Name="Setup Electrode Listbox.vi" Type="VI" URL="../support/Setup Electrode Listbox.vi"/>
		<Item Name="Setup MV Listbox.vi" Type="VI" URL="../support/Setup MV Listbox.vi"/>
		<Item Name="Toggle Control Visible State.vi" Type="VI" URL="../support/Toggle Control Visible State.vi"/>
		<Item Name="Round Timestamp.vi" Type="VI" URL="../support/Round Timestamp.vi"/>
		<Item Name="Interpolate_Group Normalized Data.vi" Type="VI" URL="../support/Interpolate_Group Normalized Data.vi"/>
		<Item Name="Interpolate_Group Timepoints.vi" Type="VI" URL="../support/Interpolate_Group Timepoints.vi"/>
		<Item Name="Interpolate_Group Interpolated Data.vi" Type="VI" URL="../support/Interpolate_Group Interpolated Data.vi"/>
		<Item Name="Extract MV Plot List Items.vi" Type="VI" URL="../support/Extract MV Plot List Items.vi"/>
		<Item Name="Plot and Scale Graph.vi" Type="VI" URL="../support/Plot and Scale Graph.vi"/>
		<Item Name="Merge Plot Legends.vi" Type="VI" URL="../support/Merge Plot Legends.vi"/>
		<Item Name="Set Plot Attributes.vi" Type="VI" URL="../support/Set Plot Attributes.vi"/>
		<Item Name="Get Plot Attributes.vi" Type="VI" URL="../support/Get Plot Attributes.vi"/>
		<Item Name="Calculate Array Size.vi" Type="VI" URL="../support/Calculate Array Size.vi"/>
		<Item Name="Measurement Description Dialog.vi" Type="VI" URL="../support/Measurement Description Dialog.vi"/>
		<Item Name="Get Plot Color.vi" Type="VI" URL="../support/Get Plot Color.vi"/>
		<Item Name="Set Normalize Points.vi" Type="VI" URL="../support/Set Normalize Points.vi"/>
		<Item Name="Add Default Plot Attributes.vi" Type="VI" URL="../support/Add Default Plot Attributes.vi"/>
		<Item Name="Create Default Plot Attributes.vi" Type="VI" URL="../support/Create Default Plot Attributes.vi"/>
		<Item Name="Set All Plots Visible.vi" Type="VI" URL="../support/Set All Plots Visible.vi"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Item Name="Cluster_Data Storage.ctl" Type="VI" URL="../typedefs/Cluster_Data Storage.ctl"/>
		<Item Name="Cluster_Electrode Selection Info.ctl" Type="VI" URL="../typedefs/Cluster_Electrode Selection Info.ctl"/>
		<Item Name="Cluster_FitData(single).ctl" Type="VI" URL="../typedefs/Cluster_FitData(single).ctl"/>
		<Item Name="Cluster_Graph Scaling Values.ctl" Type="VI" URL="../typedefs/Cluster_Graph Scaling Values.ctl"/>
		<Item Name="Cluster_Graph-Import Data.ctl" Type="VI" URL="../typedefs/Cluster_Graph-Import Data.ctl"/>
		<Item Name="Cluster_Graph-Interpolate Data.ctl" Type="VI" URL="../typedefs/Cluster_Graph-Interpolate Data.ctl"/>
		<Item Name="Cluster_Graph-Normalize Data.ctl" Type="VI" URL="../typedefs/Cluster_Graph-Normalize Data.ctl"/>
		<Item Name="Cluster_Graph-Statistic Data.ctl" Type="VI" URL="../typedefs/Cluster_Graph-Statistic Data.ctl"/>
		<Item Name="Cluster_Group Info.ctl" Type="VI" URL="../typedefs/Cluster_Group Info.ctl"/>
		<Item Name="Cluster_Interpolated Data (single).ctl" Type="VI" URL="../typedefs/Cluster_Interpolated Data (single).ctl"/>
		<Item Name="Cluster_MV SD Data (single).ctl" Type="VI" URL="../typedefs/Cluster_MV SD Data (single).ctl"/>
		<Item Name="Cluster_Normalized Data (single).ctl" Type="VI" URL="../typedefs/Cluster_Normalized Data (single).ctl"/>
		<Item Name="Cluster_Plot Attributes.ctl" Type="VI" URL="../typedefs/Cluster_Plot Attributes.ctl"/>
		<Item Name="Cluster_Settings-Interpolate.ctl" Type="VI" URL="../typedefs/Cluster_Settings-Interpolate.ctl"/>
		<Item Name="Cluster_Settings-Normalize.ctl" Type="VI" URL="../typedefs/Cluster_Settings-Normalize.ctl"/>
		<Item Name="Cluster_TER Dataset.ctl" Type="VI" URL="../typedefs/Cluster_TER Dataset.ctl"/>
		<Item Name="Enum_Interpolation Method.ctl" Type="VI" URL="../typedefs/Enum_Interpolation Method.ctl"/>
		<Item Name="Enum_Norm Resistance.ctl" Type="VI" URL="../typedefs/Enum_Norm Resistance.ctl"/>
		<Item Name="Enum_Round to.ctl" Type="VI" URL="../typedefs/Enum_Round to.ctl"/>
	</Item>
	<Item Name="GIN-UI.vi" Type="VI" URL="../GIN-UI.vi"/>
</Library>

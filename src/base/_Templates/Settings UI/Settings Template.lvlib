﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*$!!!*Q(C=\&gt;5^4C."%)&lt;B$\1"K7_!P%?IA!PY#LZ#87'%.FESRZP6&amp;?9#3(:+7&amp;@Q$=!*-@+_X60WMB,##3#%[((&lt;HK``(HL'A^4,B@2$_V0FZN6#/T0&gt;N"&gt;H`5P\H"*K'[`J6&lt;WGMU-^N%]NR`%^?&gt;Z_'+"J7&lt;UU`NCO`]?`\D^2^&amp;W_S^=OPU`_#.\^&gt;X#G^C#C*D7I4D7V:@&gt;*HO2*HO2*HO2"(O2"(O2"(O2/\O2/\O2/\O2'&lt;O2'&lt;O2'&lt;O4^)"?ZS%5/+6E]73C:.*EA[1R&amp;S6PC34S**`&amp;QKM34?"*0YEE]&gt;&amp;(C34S**`%E(I9J]33?R*.Y%A^4&gt;5HWARR0YG&amp;[":\!%XA#4_"B317?!"!M&amp;EQ=4!*$170Q*@!%HM$$6Q7?Q".Y!E`AI6G"*`!%HM!4?"D3&gt;S7[JBXE?*B'DM@R/"\(YXC97I\(]4A?R_.Y7%[/R`%Y#'&gt;":X))=A9Z(:Q4R_.Y_*$D=4S/R`%Y(JL[&amp;@+_-UX4$H)]BM@Q'"\$9XC91I&lt;(]"A?QW.YG&amp;;'R`!9(M.D?&amp;B+BM@Q'"Y$9CT+]D)G-Q9;H9T!]0$K&gt;YPVKR2&gt;9PW1[O:6X:3KGUVV%[FO$N6&amp;6VV-V563&lt;&lt;ZK5V7&lt;J&gt;I%V2_H1KMQKE65AVN(\8D@5D@5E&lt;KC$N1F&gt;5'&gt;5W&gt;N["NXX/VWWG[XWGQW'M&gt;2K^6+QT"IO6RKM6BI0J^L.JM&gt;(Q/8(-=(QL`HUNX[_O@$H^PB@PRV^&lt;"_'O\8DV=@^0`Z%TQ&lt;&gt;;\H;\"(@Q'6%73W!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Settings (public)" Type="Folder">
		<Item Name="Read Write Module Config.vi" Type="VI" URL="../Read Write Module Config.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Cluster_Data.ctl" Type="VI" URL="../Cluster_Data.ctl"/>
	</Item>
	<Item Name="Settings UI Actor.vi" Type="VI" URL="../Settings UI Actor.vi"/>
</Library>

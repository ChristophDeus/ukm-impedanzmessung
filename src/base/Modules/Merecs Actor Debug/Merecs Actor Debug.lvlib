﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">40848</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16775072</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;5P&lt;BN2%-@R8[O!5N_A]B8G#K9JC9Q##C).S!6-#XW&amp;O9+P9'9J&lt;'D#D#):&amp;/Q6H/]&lt;D:SW)%P;KKKS,S_\`LV`H\R&gt;&lt;[1[0EF8/M]&gt;BT?0U;Z$6V7NU['O.&gt;L6Z?&gt;48&gt;&lt;YU;WO+XQ&gt;8V&gt;PD\_U`\J_@[\W'@`-I@@D`@C`DW_T8Y)``DXYI0%CIC9VK%YVD7803:\E3:\E3:\E12\E12\E12\E4O\E4O\E4O\E2G\E2G\E2G\E6=B&amp;,H+21UI74R:+*EUG3$J$5@)L]33?R*.Y_+D%EXA34_**0(22YEE]C3@R*"['+@%EHM34?")05Z5EKZ$D34R-L]!4?!*0Y!E],+H!%Q##R9+*AUFA+'A-,A*0Y!E]8#LQ"*\!%XA#$]U+0)%H]!3?Q-/1WJ5IT3DE?*B'DM@R/"\(YXC97I\(]4A?R_.Y7%[/R`%Y#'&gt;":X))=A9Z(:Q0DM@R=*,D=4S/R`%Y(JLK$HHNT.#-1I\(]"A?QW.Y$!^4S0!9(M.D?!Q0U]LQ'"\$9XA-$UP*]"A?QW.!D%6:8M:ERE#DER%9(H\K;&lt;'[3V%3KS,VQ[N_+.50G`IB5D]=[JOOPJHKG[4?@07GKD&gt;,P1HK0U[.6G05C[A(DY[;_(WE\KE\[J;[I;[J+_K3OBB$@X0(;:JU0"[VX__VW_WUX7[VW7SU8K_V7KWU8#[V7#QOLY(0F-M,Y@7^.*WO\R\P(\]_@,__Y8T\&gt;0LS`*@_0`]$\U:^V)^LM%=PQ(/%ZA!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Filter Messages.vi" Type="VI" URL="../private/Filter Messages.vi"/>
		<Item Name="Filter States.vi" Type="VI" URL="../private/Filter States.vi"/>
	</Item>
	<Item Name="Cluster_Debug JKI States Infos.ctl" Type="VI" URL="../Cluster_Debug JKI States Infos.ctl"/>
	<Item Name="Cluster_Debug Messages Infos.ctl" Type="VI" URL="../Cluster_Debug Messages Infos.ctl"/>
	<Item Name="FGV_JKI States Debug Infos.vi" Type="VI" URL="../FGV_JKI States Debug Infos.vi"/>
	<Item Name="FGV_Messages Debug Infos.vi" Type="VI" URL="../FGV_Messages Debug Infos.vi"/>
	<Item Name="Global_Debug State.vi" Type="VI" URL="../Global_Debug State.vi"/>
	<Item Name="Main Debug UI.vi" Type="VI" URL="../Main Debug UI.vi"/>
</Library>

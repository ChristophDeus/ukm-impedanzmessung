﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;5`&lt;BJ"&amp;-@R8[Q5;?F&gt;7/1)L`!&amp;E(Q#KB4OXB7W='-[J*TA+4@A#P3OXB6Q*"&gt;O)N%CJ3(@'6[7O!F.9FE7MRZA@`0PQ_SSFHLZ*(X5`F2:`,7U&gt;K9;;ZXR&gt;DCFP98(F^]@KN.B`+&amp;7GR9PRS_K=\701V8L[T"?.8]B&amp;G0\#@_*IH-ZF`&gt;&gt;\E`_#0\\\_#$WI/)GN3A/N85FNUH?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:(XAVTE)B=ZJ'4R:+&amp;EUG3#J$-5*3_**`%EHM4$K2*0YEE]C3@RU%7**`%EHM34?"CGR*.Y%E`C34R-V38:$X)]C9@J&amp;8A#4_!*0)'(*26Y!E#Q7$"R-!E-"9X"B]!4?!)0(R6Y!E`A#4S"BW9&amp;HM!4?!*0Y'&amp;)XZ8IGH;1YW%;/2\(YXA=D_.B;DE?R_.Y()`D94EZ(M@D)*Q&amp;H=EBS"HE&gt;("/()`DY5W/R`%Y(M@D?'DK6]D\TD2./]DR'"\$9XA-D_&amp;B#BE?QW.Y$)`B96I:(M.D?!S0Y7%J'2\$9XA-C,%IS]O9T"BI&gt;$)#Q].@PVOM8[8I%OO(6$?P[K:5X7SKGUBV=[AOOOJCKC[3;P.6G[L;,.5GK,[=#KX#K":2$7Y&gt;N?6V1VV46^1F&gt;;$/K40KF$JJ1`^RR_VWK]VGI`6[L&gt;6KJ?6SK7%9.*`0.:P..*V/.:F-RM@!&amp;=@Y1$A_FZY_@\VZX.V^?2[_X@T=`&lt;B^O,\]`EL`H^`!MV%8_H-.^OA8;8ZE-1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="CreatePaths.vi" Type="VI" URL="../CreatePaths.vi"/>
	<Item Name="CreatePaths_nas.vi" Type="VI" URL="../CreatePaths_nas.vi"/>
</Library>

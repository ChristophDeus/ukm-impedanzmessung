﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">4944486</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;5^&lt;B.2&amp;-8R![+A&gt;5_"T"*/Q19M:17OUN]N4%&amp;$;9E68&amp;'\]2&lt;=1X/X9!I+'C3XE7D#@ZZPH.$%$3#%]C&lt;0^JTX^@/&lt;]51;Z;8U1L?8SP&lt;2-L=TV&lt;HW'7_H5^LH]0\F\E.X/IU`V7\4^N@RW_\=\??B&gt;_.V'K_?PR(&lt;B_-@]V]I?CJ0Z@]O\S`_#0\Y\_#:ZA=2N;B*$;IV,XN&lt;Z%6?Z%6?Z%7?Z%G?Z%G?Z%E?Z%%?Z%%?Z%&amp;O=J/&lt;X/1G.`EYS%5O=J&amp;$+B9P&amp;CIG,39I/E.2]6*Y#E`B+4S=KP!5HM*4?!I0862Y#E`B+4S&amp;BW%K0)7H]"3?QM.51V,D)-&gt;4?*B?C3@R**\%EXB95IEH!33,*2-HE]"1UJB]3$S**`(Q59EH]33?R*.Y;&amp;&lt;C34S**`%E(I;-8=GBG1^S0%SDQ".Y!E`A#4R-L=!4?!*0Y!E],+@!%XA#2,"A-$E%"9/#$M&amp;*Y!E]P#HQ"*\!%XA#$UXD#M89G6ET(_2YD-&gt;YD-&gt;YD)=J:$T'9TT'9TR-+_-R(O-R(O.B+2G0]2C0A:B&amp;76ZG-D01&gt;$+"]@!X\B;0KZ2$YH&amp;)@@/K&lt;ULVT;;_C&gt;1XB`KCKS_G_C+J.V_^K?L.5G_#_MOJU7K-?B(VY,GDDLQ?K(PKDLKB4N1V&gt;56&gt;5B@TU.`=]8A][H!Y;,`@;\@&lt;;&lt;0:;*IGL&gt;&gt;LL69L,:&gt;,,2;,]W0A.=@ZA8$`80L[ZM06FZNXV^_GDV=`&lt;LZ@@XL\[P.@_P`]$TQ&lt;^6Q0VW#0@A+U,X?K!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Events" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MSG_Generate Settings Finished.vi" Type="VI" URL="../MSG_Generate Settings Finished.vi"/>
	</Item>
	<Item Name="Public UIs" Type="Folder">
		<Item Name="Settings Subpanel.vi" Type="VI" URL="../Settings Subpanel.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Cluster ShowProperties.ctl" Type="VI" URL="../Cluster ShowProperties.ctl"/>
		<Item Name="Enum_Settings Run Mode.ctl" Type="VI" URL="../Enum_Settings Run Mode.ctl"/>
	</Item>
	<Item Name="Start Settings Subpanel Asynchron.vi" Type="VI" URL="../Start Settings Subpanel Asynchron.vi"/>
	<Item Name="Show Settings.vi" Type="VI" URL="../Show Settings.vi"/>
</Library>

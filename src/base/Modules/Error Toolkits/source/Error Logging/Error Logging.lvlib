﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16755616</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12713864</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)`!!!*Q(C=\&gt;7R&lt;&gt;N1%-&lt;R,U;+6!1UA!&amp;$+^Q)ZA1'N-)NY%+N3KVQA"I(3#-A64JTB6O"+WA&amp;_M_HA[+E-*ME#!S2@J4U0&lt;ZX0TV3N.3W,^*H45N&lt;^_Z'`V4N@/CGCLPK[#K=TI&gt;@0N&lt;Y\P@RV&lt;]Y@LL5P_I`4`7T`I*`9&gt;.NOWU@?^MN`AD__O`AE_9(%3VJ18/;;3Y\*8G3*XG3*XG3"XG1"XG1"XG1/\G4/\G4/\G4'\G2'\G2'\G2NZV=Z#)8/;3E?&amp;)IG43:)$E:CJ*$YEE]C3@R]&amp;'**`%EHM34?$B&amp;C3@R**\%EXA9JM34?"*0YEE]4.5EW8:S0)G([26Y!E`A#4S"BZ)+0!%A+":-(%Q#1U&amp;H]#&lt;Q"*\!QVM&amp;HM!4?!*0Y+&amp;&lt;A3@Q"*\!%XA9UF9FGG&lt;?S@%QD2S0YX%]DM@R-,5=D_.R0)\(]6"/DM@R/!CHI$-Z"$G$H"/=$Y\(]@!CR_.Y()`D=4RUN3PE&lt;76GT&lt;S4YT%]BM@Q'"\$QR1S0)&lt;(]"A?Q]/U-DS'R`!9(M.$+2E?QW.Y$)B2F0)S*D-''C=:A?(BL^UNVKZ3.)GV8;K&lt;6X64KGYWV5WEODF5&amp;VVV-6583&lt;8YKE66,::K%62@4I67962&amp;6)0H%X8C/.)'WJ'WJWVJ'VJ07^.7]^!`@/,J&gt;.)YDBK'1=@D5@P^8NPN6JP.2HX@;\V?;\6;82Y$$_S8"],ZO&lt;4D`8"YP0`[V,`]/04@?@XW_P2]0_@`Y0`T@`"MV*WO;\"'&lt;Y4C6%)!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Create Or Open Log File.vi" Type="VI" URL="../Create Or Open Log File.vi"/>
		<Item Name="Create Log Row.vi" Type="VI" URL="../Create Log Row.vi"/>
		<Item Name="Update Header.vi" Type="VI" URL="../Update Header.vi"/>
		<Item Name="Write File.vi" Type="VI" URL="../Write File.vi"/>
	</Item>
	<Item Name="Write To Error Log.vi" Type="VI" URL="../Write To Error Log.vi"/>
	<Item Name="Delete old Log Files.vi" Type="VI" URL="../Delete old Log Files.vi"/>
	<Item Name="Reduce File Entrys.vi" Type="VI" URL="../Reduce File Entrys.vi"/>
</Library>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">7641720</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16383904</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)7!!!*Q(C=\&gt;3R=1)R%)8B:Y]$JX4AI96NA2:I96MA)(&amp;)#^M#,&gt;$#;Y%7;!(`*X9!*_$!HG%][#TA(J,WM_YY;&lt;2X[5X(?WV^MZW'D'%^&gt;JTT&gt;BW?TPP^W`HR-OI]@XVL`PIS^;L_V@KH#?&gt;+0`(@;HKW:`P@\@0OD_$0@Q=PGBZ%&gt;./,HP4160:I=J/&lt;X/1G.XG2&amp;XG2&amp;XG2&amp;XG3*XG3*XG3*XG1"XG1"XG1"`EYS%5O=J&amp;$-M6.)&lt;/I7=!-BC,T9DT'9TT'Q[G-RXC-RXC-BS%S(O-R(O-R(K&lt;*?)T(?)T(?&amp;BK3$Q/=DT'Q`)K0)7H]"3?QE.*&amp;:Y#5"1L&amp;CY7A;(CS_*$Y3E]B9?0+DS&amp;J`!5HM,$VSI]B;@Q&amp;*\#QZ3R+T5UUU'/BW75?"*0YEE]C9?FF8A34_**0)G(=EI]C3&gt;"*!74R3%IG:1-3%Y34_,B49EH]33?R*.Y_'J=I2Q\-WGGARR0Y!E]A3@Q""[75/!*0)%H]!1?FF8A#4S"*`!%(EIJ]!3?Q".!AK+56\"9-$%9&amp;!3"B\^RN]3Y3D5E-1[J&lt;V\V4;G_W&gt;1XE@LG5&amp;^U^=658S4VZKMX6&lt;V:[EV1`X.KN"KD,K+?0!X5A&gt;=^@5@@UD@U&amp;8V*8^$H^.EU^:=((AY(\@&gt;\\89\&lt;&lt;&gt;&lt;&lt;49&lt;L69L,:&gt;,,29,T?&gt;TT7;T]W0AA_0]1(C%Z^*$'&amp;ZV89-^_A*CV+4W!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Test VIs" Type="Folder">
		<Item Name="UnitTest_MergeHeader&amp;Arguments.vi" Type="VI" URL="../UnitTest_MergeHeader&amp;Arguments.vi"/>
	</Item>
	<Item Name="CreateTableBasedOnCurrentHeader.vi" Type="VI" URL="../CreateTableBasedOnCurrentHeader.vi"/>
	<Item Name="ErrorToSpreadsheet.vi" Type="VI" URL="../ErrorToSpreadsheet.vi"/>
	<Item Name="RemoveOrReplaceEndOfLine.vi" Type="VI" URL="../RemoveOrReplaceEndOfLine.vi"/>
</Library>

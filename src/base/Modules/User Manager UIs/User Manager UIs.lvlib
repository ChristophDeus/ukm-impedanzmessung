﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">3079936</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">11507131</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)^!!!*Q(C=\&gt;7R&lt;&gt;N!&amp;-&lt;R,U'+N!)S1+!6XAJK.)"7?(UKNCZ3K("&amp;1-7XAF&lt;1#G]&amp;V?GUAP+`S^%S9-.KEC!)@.3*UM?\&gt;T_4&amp;#XV^FH[J/O^&gt;HCTN?0^N?Q0&lt;=L38TH?[VU0N^%PZS`^V@G(J=BN^$,I.KWP@TP_JP^/UXN\&lt;`^X?\D\)`DDPY-0;A]C?N&amp;.4XKI,8MN]C)P]C)P]C)XO=F.&lt;H+4GTT*ETT*ETT*ETT)ATT)ATT)A\RPZ#)8O=AB&amp;9M8#R6&amp;CQ,&amp;9#AKXAJ0Y3E]B9?P+DS&amp;J`!5HM,$%"7?QF.Y#E`B9:I+4_%J0)7H]&amp;#K3[JPZ(A+$_6F0-:D0-:D0#QJYT%!MZAJ&lt;)L!E$FI0BC0]2A0(W5]RG-]RG-](*&lt;R')`R')`R-+7@&amp;8&gt;.W]DR5%;**`%EHM34?#CNR*.Y%E`C34QMJ]34?"*%MG"3()+33=G!Z%PC34TMF(A34_**0)G(1`U+:4]T4&gt;-W=DS"*`!%HM!4?#CBQ".Y!E`A#4S56?!*0)%H]!1?FF,A#4S"*Y!%C\+]AG,"R'"1%!1?8PVOC8[6X#82.WH=P"IXJ=&lt;.JH%4;&gt;Q='B&gt;&gt;YW*K8#3.E[^R5D6/FM:*U0DD..!;')V&amp;.#;XA&lt;LQ@K;@[%@[HD\2&gt;`1.@5V@N;G`??$F=N(Z@.&lt;J&gt;.,R?.2_P^=U4&gt;LN&gt;NJM.FKPVVKN6E_0A;^M4Q_%8]_F"T\0]`2NHL^PZTH:@^E_0P\9NPQP`(`_"Z[._KDH;X#/@A)T(J`$!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Exit Button Position.vi" Type="VI" URL="../Exit Button Position.vi"/>
		<Item Name="RW User Config.vi" Type="VI" URL="../RW User Config.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Cluster Template Settings.ctl" Type="VI" URL="../Cluster Template Settings.ctl"/>
		<Item Name="Enum ActionUserManager.ctl" Type="VI" URL="../Enum ActionUserManager.ctl"/>
		<Item Name="Cluster LogIn-Properties.ctl" Type="VI" URL="../Cluster LogIn-Properties.ctl"/>
	</Item>
	<Item Name="Public UI" Type="Folder">
		<Item Name="UI_Settings.vi" Type="VI" URL="../UI_Settings.vi"/>
		<Item Name="UI_Change Password.vi" Type="VI" URL="../UI_Change Password.vi"/>
		<Item Name="UI_LogIn.vi" Type="VI" URL="../UI_LogIn.vi"/>
	</Item>
	<Item Name="Send" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="MSG_Show LogIn UI.vi" Type="VI" URL="../MSG_Show LogIn UI.vi"/>
	</Item>
	<Item Name="Reply" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MSG_Login OK.vi" Type="VI" URL="../MSG_Login OK.vi"/>
		<Item Name="MSG_Generate Shut Down.vi" Type="VI" URL="../MSG_Generate Shut Down.vi"/>
	</Item>
</Library>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10452224</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16757617</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)^!!!*Q(C=\&gt;7\&lt;2N"&amp;)8B9]'"5H9A-(6Y7[!\]-&lt;-&lt;O;9K5/W=!%L&amp;E#"&amp;&lt;!#';=#!7S",=D`D-9U&lt;6BC9AG'I&gt;E&gt;;P@M0$\/,F&gt;3,Z@3?TW=+]NH#^?FNP?[Z("ZX&amp;P3LD`'/DX1DY0@_`?4ER:0^6`_WP`0VR`H0_-`5`27XML`8&lt;[=`2']_/`AH&gt;K,C'JK5:-;;N-_G.TE*D?ZS5V?Z%6?Z%6?Z%7?Z%G?Z%G?Z%E?Z%%?Z%%?Z%(?.X+2CVTEE-TE:C)TK"H!.)9C]W%]RG-]RM/JD-&gt;YD-&gt;YD)=G-B\D-2\D-2[[S8C-RXC-RXA9KEP=.X)]RM0Q+DS&amp;J`!5HM,$F#I]";#9L"CY'!3'CIP&amp;1?%J0)7(1R7?QF.Y#E`BY&lt;)+4_%J0)7H].#FLUJV4&gt;P)]4#-%E`C34S**`%QN"*0YEE]C3@R-*U34_**%-G%S?!1F(2+'C1HC3@R]%?**`%EHM34?,D5\V$WF7G;NJ(D#4S"*`!%HM$$%!I]A3@Q"*\!Q\!+0)%H]!3?Q-.5#DS"*`!%E'"3JF=Q7.!R;"1%A9?^0SX2\V*V3@2.'A_PRE/J]&lt;"J0%1;$Y@'4&gt;?YG2IX370R.2:69\%U&amp;E(DSWGA.4!;EWBU&lt;AVVY(.0X6%XV$6V2:WI#_K=/GN&gt;`X,$Q_'A`8[PX7[HT7;D^8KNV7KF;:KU7#QUH]]VG]W/LY%LNO-,Y?&gt;\[?\[]]X^^N0NX&gt;?0(_[XU_WX\84T3P_@`Y&amp;XISZU/A&gt;L^"W4P5K)!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="buffer" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Error Cluster to Error Name String.vi" Type="VI" URL="../private/Error Cluster to Error Name String.vi"/>
		<Item Name="Get Error from Buffer.vi" Type="VI" URL="../private/Get Error from Buffer.vi"/>
		<Item Name="Add Error to Buffer.vi" Type="VI" URL="../private/Add Error to Buffer.vi"/>
		<Item Name="Delete Error from Buffer.vi" Type="VI" URL="../private/Delete Error from Buffer.vi"/>
		<Item Name="Error Buffer.vi" Type="VI" URL="../private/Error Buffer.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Get Time Stamp.vi" Type="VI" URL="../private/Get Time Stamp.vi"/>
		<Item Name="Is Undefined Status.vi" Type="VI" URL="../private/Is Undefined Status.vi"/>
	</Item>
	<Item Name="Events" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Event_Generate Error Occurred.vi" Type="VI" URL="../private/Event_Generate Error Occurred.vi"/>
	</Item>
	<Item Name="type defs" Type="Folder">
		<Item Name="Cluster_BufferData.ctl" Type="VI" URL="../Cluster_BufferData.ctl"/>
		<Item Name="Cluster_Parameter.ctl" Type="VI" URL="../Cluster_Parameter.ctl"/>
	</Item>
	<Item Name="globals" Type="Folder">
		<Item Name="Global_StringConstants.vi" Type="VI" URL="../Global_StringConstants.vi"/>
	</Item>
	<Item Name="Unit Tests" Type="Folder">
		<Item Name="UT_MaxErrorLogSize.vi" Type="VI" URL="../UT_MaxErrorLogSize.vi"/>
	</Item>
	<Item Name="Main Error Handler.vi" Type="VI" URL="../Main Error Handler.vi"/>
	<Item Name="Set Parameter.vi" Type="VI" URL="../Set Parameter.vi"/>
	<Item Name="Get Buffer.vi" Type="VI" URL="../Get Buffer.vi"/>
	<Item Name="Exit Error Handler.vi" Type="VI" URL="../Exit Error Handler.vi"/>
	<Item Name="Get Error Log Folder Path.vi" Type="VI" URL="../Get Error Log Folder Path.vi"/>
</Library>

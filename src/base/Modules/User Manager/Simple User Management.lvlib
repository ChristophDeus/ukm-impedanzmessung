﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10444544</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16773906</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)P!!!*Q(C=\&gt;8041*2%-@RH];$6TIQ&gt;'#G"6LA\'V;Y)D(47T!;9%7NI7ZKQ=MA2*9P`M=Q%35CRJD@/MA`0&lt;^_@BW7;87,K5,$;@;T;&gt;N&gt;V[P0^+_DJR8P;P?R]&lt;P[ODYG^UE\]=@BL8VXZ``U(_C[&lt;`^N\`&gt;&lt;E^_#&lt;\^?X#G]5&amp;%*2755[:RW3(*ETT*ETT*ETT)ATT)ATT)A^T*H&gt;T*H&gt;T*H&gt;T)D&gt;T)D&gt;T)D&lt;Q&gt;Z#)8O=AB*9MH#S74*B-EH;%I?5E]C3@R*"Y_+P%EHM34?")08:2Y%E`C34S*BW&amp;+0)EH]33?R-.54:,N)-?4?*B?A3@Q"*\!%XB95I%H!!3,"2-(E]"1=$*Y%XA#4_$BL1*0Y!E]A3@Q=&amp;K"*`!%HM!4?"D3&gt;C7;:DT)]4#.()`D=4S/R`%QN2S0YX%]DM@RM*Q=D_.R%-[#TO11Z!RS/DA@()`DY:==D_.R0)\(]8#K83&amp;P/T.KRI-=D_%R0)&lt;(]"A?JJ$B-4S'R`!9(K;6Y4%]BM@Q'"[7EO%R0)&lt;(A"C,MLS-S9S"2C=D-$T]N,P&amp;WF7+*L&amp;W3(8TKGZ+V=WGOIF5.Y@KIKMOJOICK4:@N;GKT6*NAOK05[&amp;6'.5CKM&amp;D2WVY86-^N;)[;E(.K2EVJ3&lt;DU#`OO.FMN&amp;[PV@?^6KO6OK\49L(1@$\8&lt;$&lt;4&gt;$L6:$,:0Q;O/090B-.T;@P9$&gt;O(@LF^OB_WT`VSO,Z&lt;`N$`ZV`Q&lt;.3ZXK\"(LU!8!"9JA!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="FGV User Data.vi" Type="VI" URL="../FGV User Data.vi"/>
		<Item Name="RW User encryted file.vi" Type="VI" URL="../RW User encryted file.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Load UserSettings from Disk.vi" Type="VI" URL="../Load UserSettings from Disk.vi"/>
		<Item Name="Get User Settings.vi" Type="VI" URL="../Get User Settings.vi"/>
		<Item Name="Check User.vi" Type="VI" URL="../Check User.vi"/>
		<Item Name="Get Current User.vi" Type="VI" URL="../Get Current User.vi"/>
		<Item Name="Set User Settings.vi" Type="VI" URL="../Set User Settings.vi"/>
		<Item Name="Save UserSettings To Disk.vi" Type="VI" URL="../Save UserSettings To Disk.vi"/>
		<Item Name="Disabled State On User Level.vi" Type="VI" URL="../Disabled State On User Level.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Cluster User.ctl" Type="VI" URL="../Cluster User.ctl"/>
		<Item Name="Cluster UserManager.ctl" Type="VI" URL="../Cluster UserManager.ctl"/>
		<Item Name="Enum UserType.ctl" Type="VI" URL="../Enum UserType.ctl"/>
	</Item>
</Library>

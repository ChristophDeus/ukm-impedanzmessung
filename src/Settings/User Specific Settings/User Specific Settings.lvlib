﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*"!!!*Q(C=\&gt;7^&lt;2N"%)&lt;BTY9#J_T!).4"N%$!&amp;&lt;!$;U)J*+")F\'&amp;;9'!+G!,UQ)&lt;%/%DY)#J^/ZCR+-3-&lt;%.Q&gt;!OFTJ_NT]0^YYHK:&gt;PUJ7?,Z8BX&gt;,/CTJ)^2KG9UXH7[\8LM05JP($W@B_`0&lt;];[`K=$\_V&amp;]HSJPR\`MP&amp;(W7T`*`FY?,0Y+``DPYIP9AIC5N;%YTN77@ETT*ETT*ETT*ATT)ATT)ATT)H&gt;T*H&gt;T*H&gt;T*D&gt;T)D&gt;T)D&gt;T)?S58O=B&amp;$CF:0&amp;EIG43:)/E-2=F&lt;YEE]C3@R]&amp;'**`%EHM34?/CCR*.Y%E`C34Q-5_**0)EH]31?JOK3\*5=4_*B?A7?Q".Y!E`A95E&amp;HA!1,":-(%Q#1](*Y#$Q"*\!Q[%#4_!*0)%H](";A3@Q"*\!%XA9UH=FOK:6=DR-)]@D?"S0YX%]4#X(YXA=D_.R0#QHR_.Y()3TI$-Z"$G$H!\/"]@D?0ADR_.Y()`D=4S=[F@)_]YU4;PE?!S0Y4%]BM@Q-)5-D_%R0)&lt;(]$#N$)`B-4S'R`#QF!S0Y4%]"M29F/6F4'9-.$I:A?(BV?]7[V=JOM2[F?LG6&gt;W5KJN.&gt;2/J&lt;A\626&gt;&gt;4.6&amp;5GW_;F.6G[8;".785[&amp;6'.5CKM'NIU&lt;?&gt;\1N&lt;5.&lt;UV;U*7V"G^.G&lt;?A@\DC/IX;\H&lt;&lt;&lt;L4;&lt;D&gt;&lt;LN6;LF:&lt;,J2;,B?&lt;TO7;TW?ER]*V[?C"-T[8^^?]@4\^O$`O@R\P^Y`XB?/.X`_D`]Q&gt;Y.OKLTN&gt;ADVY!LBZG(Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Settings (public)" Type="Folder">
		<Item Name="Read Write Module Config.vi" Type="VI" URL="../public/Read Write Module Config.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Default Config User Settings.vi" Type="VI" URL="../support/Default Config User Settings.vi"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Cluster_User Settings.ctl" Type="VI" URL="../typedefs/Cluster_User Settings.ctl"/>
		<Item Name="Cluster_Graph Scaling.ctl" Type="VI" URL="../typedefs/Cluster_Graph Scaling.ctl"/>
	</Item>
	<Item Name="User Specific Settings UI.vi" Type="VI" URL="../User Specific Settings UI.vi"/>
</Library>

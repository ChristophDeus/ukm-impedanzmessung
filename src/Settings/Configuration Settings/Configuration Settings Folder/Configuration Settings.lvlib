﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;7R&lt;2N"%)8B:]/"5X:AM)6JA1I.2QS:$G!\=-B15M:-]5!&gt;M!7W]&amp;JA#;)#&amp;3$`NRR1AA#,C7U9BO[Y2`,N\OSHO_.*'NN([9-?TWX8LW[D8_L$?&amp;&amp;Z@"DJV(^-DVX(4XV][D].ELL1]@XF`/PH]`7,^&lt;P;;@\L`D/&lt;XL;X\@`?LM\_#0\Y\_#&gt;JA=2T&lt;3C*3UU,@NI=J/&lt;X/1G.XG2&amp;XG2&amp;XG2&amp;XG3*XG3*XG3*XG1"XG1"XG1"`H9S55O=J&amp;$-IO&lt;B5R25]!-BC*T-"\D-2\DY;O-RXC-RXC-BS%S(O-R(O-R(K&lt;*?)T(?)T(?#AV*"Y\/2\DI&lt;Q+4_%J0)7H],#E#E]"+"9L#B&gt;&amp;9+DI,$Y5HM*4?0CIQF.Y#E`B+4RUK`!5HM*4?!I05]::K;':&gt;H)]F&amp;(C34S**`%E(EIL]33?R*.Y%A`,+@%EHA32,*A5B["E5D)A_:*Y%A^P3DS**`%EHM2$V\B#/=\-J*FW=DS"*`!%HM!4?#CBQ".Y!E`A#4S56?!*0)%H]!1?FF,A#4S"*Y!%C\+]AG,"R'"1%!1?8O.OC8'6;EBC\&amp;,@P/K&lt;5HWTK7]C^=WBPODKC[G_3/K4LT[J[J/F0AHK0U[.6G05C[AH4Q.VY,CH\7B&lt;WI;WJCVJ#^K=.JOG`O;"B]."_`V?O^V/W_V7G]V'[`6;S_63C]6#]`F=M^HM^"DYR(Z[)$Q^FRZO,W\OPKYO\\^^`H,X@@8DY8:V]Z@_0`]$TU;^V`-V/%=`!:-=9_5!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Settings (public)" Type="Folder">
		<Item Name="Read Write Module Config.vi" Type="VI" URL="../../public/Read Write Module Config.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Default Config.vi" Type="VI" URL="../support/Default Config.vi"/>
	</Item>
	<Item Name="Configuration Settings UI.vi" Type="VI" URL="../../Configuration Settings UI.vi"/>
</Library>

﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Y!!!*Q(C=\&gt;7^E&gt;.A&amp;)8B!U.![IS1=1OX"&lt;@A&amp;GY,$ED94"HR;=%N/#3^(8A]4IBA2CUMLTZ@D$&gt;AH1$$-#PNV6LH_XN7EL83W.Z+&lt;`2Y&lt;XNY&gt;FP;^4"+`5G[6L=P*\&gt;&gt;&lt;A[8]2INFR'DKZ[W0RF][@WL&gt;OF[_.(_P0`/JJ@N:@O`NY^XPQ2``(PQ3MO,C#L+6&amp;+B:&gt;H()C`S)C`S)C^SEZP=Z#9XO=G40-G40-G40-G$0-C$0-C$0-D(4CZSE9M=5L&amp;YM6!R;4&amp;"U2G+CE0B+4S&amp;J`"QKM*4?!J0Y3E]&gt;&amp;(B+4S&amp;J`!5(I;J]"3?QF.Y#A^4$5G.H2R0Y7&amp;['9`R')`R'!^,SHA-Q#RG*D;4Q*"J."_-RXC-BY]S(O-R(O-R(JJF0-:D0-:D0!Q:6]6$M_TE?*B'C3@R**\%EXC97IEH]33?R*.Y7%[**`%EC'4":())3A9F(:+4R*.Y_+8%EXA34_**0$3./Z4DSCS;:3@(%XA#4_!*0)'(+22Y!E`A#4S"BWE6?!*0Y!E]A9?F&amp;(A#4_!*)-'C,+^AMG"AU#E)!A]`YWG*=:=]*$&amp;WK2^?^5/J@ND5$Z([Y6$@&gt;08.6.]E^=688V4VR6*@"05@JU;L-?J&amp;V)/8DJIZHKA$N;=G;E&gt;NK1WVJF&lt;,U.`==:ZHH5YH(1Y(\@&gt;\4&gt;/EX7[H\8;LT7;D^8KNV7JV@1W]:\__%([_F_&lt;D^/H,]=0Z[W?@Z_-X[NXZ,`V``A@?D8KNWT7Y2N]"-VVK-Q!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Settings (public)" Type="Folder">
		<Item Name="Read Write Module Config.vi" Type="VI" URL="../public/Read Write Module Config.vi"/>
	</Item>
	<Item Name="Meas Hardware Settings UI.vi" Type="VI" URL="../Meas Hardware Settings UI.vi"/>
</Library>

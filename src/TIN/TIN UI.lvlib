﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16754080</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16758977</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)K!!!*Q(C=\&gt;7^&lt;2N"%-8R:]/")Q0MQ'!,LQ7WQ";G";:ERM#291O9&amp;NA#7ZD1+6NA#`2`VW0##KR,*-%Q&gt;+OBS,&gt;@0^U&gt;4^)]0EI@&gt;&amp;M[0DVZ`/Y@!U?\`^Q?^X&gt;Y7_C@HZ`K8ZI`]V\E5@^@`1O(XI[XY`]_^IN@AB@`(LT4?""22356F$7WP26ZE2&gt;ZE2&gt;ZE3&gt;ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE*P=Z#9XO=F.0BOZS%5O=ED&amp;ZM6'R;,&amp;!M6A+#J?#E`B+4S&amp;BY]K0)7H]"3?QM-1&amp;:\#5XA+4_&amp;BGAJ0Y3E]B;@QM.35V'TE?!I0SSPR**\%EXA3$VMK]33!:,.EY712'%I[ET?**`%E(NYK]33?R*.Y%A`&gt;3DS**`%EHM4$F(F7=GJ')]@$-AI]A3@Q"*\!Q^)+0)%H]!3?Q-.W#DS"*U!%'Q;,1V!Q+2A1@!A]A9&gt;@#DS"*`!%HM"$V\R#-=`-U)R'DM&gt;YD-&gt;YD-&gt;Y7%,'9TT'9TT'Q\)S(O-R(O-R(L;3]2C0]2C)W:4N:29T%]UA%RA00`.O]&lt;R+/37?4?K&lt;6XV4KG]W^5WEPDH5&amp;VV^-&gt;5838XSV3&gt;6@&lt;,5*U(^R[H2;IR[%`8E-6"88C`5G4J22WJ(&lt;;E.N;:79_IT$\R?L\J=,DK@TTK&gt;4DI?D^LN&gt;NJON^JM.FKPVVKN6P@(Q'@;`9(Q[\GUZ`XBR^@$`I([4HU&lt;^?5Q]F@Y``Q00"PV8H`OQ4H[#4A,0W)!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="support" Type="Folder">
		<Item Name="Export TIN Data to TDMS.vi" Type="VI" URL="../support/Export TIN Data to TDMS.vi"/>
		<Item Name="Fit Logik_old.vi" Type="VI" URL="../support/Fit Logik_old.vi"/>
		<Item Name="Get Electrode Number.vi" Type="VI" URL="../support/Get Electrode Number.vi"/>
		<Item Name="Get Plot Names.vi" Type="VI" URL="../support/Get Plot Names.vi"/>
		<Item Name="LoadData.vi" Type="VI" URL="../support/LoadData.vi"/>
		<Item Name="Write TDMS Data.vi" Type="VI" URL="../support/Write TDMS Data.vi"/>
	</Item>
	<Item Name="TIN" Type="Folder">
		<Item Name="Bool2Hex.vi" Type="VI" URL="../Bool2Hex.vi"/>
		<Item Name="CalcSpec_FGV.vi" Type="VI" URL="../CalcSpec_FGV.vi"/>
		<Item Name="CPERRC_Function.vi" Type="VI" URL="../CPERRC_Function.vi"/>
		<Item Name="FIT_CPERRC.vi" Type="VI" URL="../FIT_CPERRC.vi"/>
		<Item Name="FitWithDiffGuessedParameters.vi" Type="VI" URL="../FitWithDiffGuessedParameters.vi"/>
		<Item Name="Format_Spectrums.vi" Type="VI" URL="../Format_Spectrums.vi"/>
		<Item Name="GraphicalApproximation.vi" Type="VI" URL="../GraphicalApproximation.vi"/>
		<Item Name="Hex2Bool.vi" Type="VI" URL="../Hex2Bool.vi"/>
		<Item Name="Include_Fits.vi" Type="VI" URL="../Include_Fits.vi"/>
		<Item Name="Plot_FGV.vi" Type="VI" URL="../Plot_FGV.vi"/>
		<Item Name="Point_FGV.vi" Type="VI" URL="../Point_FGV.vi"/>
		<Item Name="RenameChannels.vi" Type="VI" URL="../RenameChannels.vi"/>
		<Item Name="Spectrums_FGV.vi" Type="VI" URL="../Spectrums_FGV.vi"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Item Name="Enum_CalcSpec FGV-Mode.ctl" Type="VI" URL="../typedef/Enum_CalcSpec FGV-Mode.ctl"/>
		<Item Name="Enum_Plot FGV-Mode.ctl" Type="VI" URL="../typedef/Enum_Plot FGV-Mode.ctl"/>
		<Item Name="Enum_Point FGV-Mode.ctl" Type="VI" URL="../typedef/Enum_Point FGV-Mode.ctl"/>
		<Item Name="Enum_Spectrums FGV-Mode.ctl" Type="VI" URL="../typedef/Enum_Spectrums FGV-Mode.ctl"/>
	</Item>
	<Item Name="TIN-UI.vi" Type="VI" URL="../TIN-UI.vi"/>
</Library>

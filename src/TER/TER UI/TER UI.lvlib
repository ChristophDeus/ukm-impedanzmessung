﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">2789375</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">7463935</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)/!!!*Q(C=\&gt;4.51*"%-8RJ_8"+RF9J0"3)!63["3Y?C3&amp;LD)#5C#&amp;4I%53'(^T^CC&amp;_7A6FG7MQSQ&lt;\Z_T#YLT8)PX7GZ6JY_,&gt;U__LV7TM:L?&gt;`_%LV_@.#_0(VR`(,JVJ^P\2`\LR4^F``SN]PDV4`"D`]0&lt;D1?2.3C*D7IVFBW+@)C,`)C,`)C4`)E4`)E4`)E$`)A$`)A$`)A.\H*47ZSEZN](O1C&amp;\H))27,&amp;QM6ER94&amp;*WBK(AL0)7H]"1?4F6Y#E`B+4S&amp;BSYK0)7H]"3?QM-Q&amp;:\#5XA+4_&amp;BKCGJ?:$D+4R-L]34?"*0YEE],+H%EQ#3R:+*EUFA+'F-PC3?R*.Y_+L%EXA34_**0$1L]33?R*.Y%A^$ZK\EV)S$(!`4+0!%HM!4?!)05SPQ"*\!%XA#$]MJ]!3?!"%M'%Q/1='AI%.Q%HA#$R]+0)%H]!3?Q%04P%)R&gt;W:IRE'/RXC-RXC-RXC91M:D0-:D0-&lt;$N$)?YT%?YT%?FJ,R')`R')B:F/6F*D-$43=4'!_P?&lt;&gt;Y8K7=%M^$[JN8@6/K&lt;T&lt;V4;3_/&gt;1888URV2&gt;*P@HK466PFHI4V$^/D6:DV)OI"Y_//P._IB[J"_K?OK.OK2PKGLI;1\_ZY`F]VOFUUP&amp;YV/&amp;QU([`VW[XUX;\V7;TU8K^VGKVODQ'(DAO$Y4@]&amp;T[&amp;9:&lt;P6_$08I'3-OQ$!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="send" Type="Folder">
		<Item Name="MSG_Active Channel.vi" Type="VI" URL="../send/MSG_Active Channel.vi"/>
		<Item Name="MSG_Measurement Started.vi" Type="VI" URL="../send/MSG_Measurement Started.vi"/>
		<Item Name="MSG_Measurement Stopped.vi" Type="VI" URL="../send/MSG_Measurement Stopped.vi"/>
		<Item Name="MSG_Process Remaining Finished.vi" Type="VI" URL="../send/MSG_Process Remaining Finished.vi"/>
		<Item Name="MSG_Process Remaining Started.vi" Type="VI" URL="../send/MSG_Process Remaining Started.vi"/>
		<Item Name="MSG_Processed Sweep Info.vi" Type="VI" URL="../send/MSG_Processed Sweep Info.vi"/>
		<Item Name="MSG_RoundNumber.vi" Type="VI" URL="../send/MSG_RoundNumber.vi"/>
		<Item Name="MSG_Test Sweep Info.vi" Type="VI" URL="../send/MSG_Test Sweep Info.vi"/>
		<Item Name="MSG_Waiting.vi" Type="VI" URL="../send/MSG_Waiting.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Create_TDMS_Files.vi" Type="VI" URL="../support/Create_TDMS_Files.vi"/>
		<Item Name="FGV_Measured-Channels.vi" Type="VI" URL="../support/FGV_Measured-Channels.vi"/>
		<Item Name="FGV_Wait.vi" Type="VI" URL="../support/FGV_Wait.vi"/>
		<Item Name="Get Channel Info.vi" Type="VI" URL="../support/Get Channel Info.vi"/>
		<Item Name="Get Last Measurement Data.vi" Type="VI" URL="../support/Get Last Measurement Data.vi"/>
		<Item Name="Get Measurement Description.vi" Type="VI" URL="../support/Get Measurement Description.vi"/>
		<Item Name="Get PC Name.vi" Type="VI" URL="../support/Get PC Name.vi"/>
		<Item Name="Get PXI Slots.vi" Type="VI" URL="../support/Get PXI Slots.vi"/>
		<Item Name="Get Relais Info.vi" Type="VI" URL="../support/Get Relais Info.vi"/>
		<Item Name="Get TDMS FileNames.vi" Type="VI" URL="../support/Get TDMS FileNames.vi"/>
		<Item Name="Get TER History.vi" Type="VI" URL="../support/Get TER History.vi"/>
		<Item Name="Text Prompt Dialog.vi" Type="VI" URL="../support/Text Prompt Dialog.vi"/>
		<Item Name="WFM Tone Measurement.vi" Type="VI" URL="../support/WFM Tone Measurement.vi"/>
	</Item>
	<Item Name="TER-UI.vi" Type="VI" URL="../TER-UI.vi"/>
</Library>

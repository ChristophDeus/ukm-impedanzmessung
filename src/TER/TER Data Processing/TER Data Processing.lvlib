﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16776704</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16252832</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)V!!!*Q(C=\&gt;5^&lt;B.2&amp;-8R!])3L5M[]!\1X9),3I1U7\B&lt;=*NS+&amp;/E/')(XI+X=,@A,8A,ZD_0[Z!%B"N!#/7^X'2]ZHX]-D/:3+/^FF\J@+WN@NG7]Z&gt;;&gt;:W@VA`^-P*HYS\(KS@(D_9`XKP00]C_E[\[LT1^N_@W@\?&lt;KX]%@`TPY)77&amp;R&amp;6F+GE1MOWZS)P]C)P]C)P=J/&lt;X/1G.\H*ETT*ETT*ETT*ATT)ATT)ATT)2S=8O=B&amp;$KH9P.CI7,29I"A-2=7XQF.Y#E`BY;-+4_%J0)7H]$"%B;@Q&amp;*\#5XC9JM*4?!J0Y3E],$5E.4IZHM,$]D)?YT%?YT%?NJ4R')$:T#RM&amp;I%B=^)=')`R'!_(-B\D-2\D-2Z/SXC-RXC-RXC9-K[+BW&lt;JZ(B92IEH]33?R*.Y7&amp;K**`%EHM34?.B/C3@R*)BEQW2R#%IG*1/3$YEH]@"$C3@R**\%EXAY.?Z1DCOT;*:/DC@Q"*\!%XA#$UMI]!3?Q".Y!A`,+P!%HM!4?!)07SHQ"*\!%U##4&gt;F?Q7,"R'"1%!1?PM&lt;4%O-O?5BC&gt;+E@8P6$K8\9V!_2_O&amp;1XX4VT64@*08&amp;6V^5^=638Q4V,[&gt;'KT(K4&gt;34FY%[]@V)(;A^.6-\;K+WV):;,V.`]]$4[;4D];D$Y;$^@K^ZHL8&lt;\42.E\&lt;&lt;L4;&lt;D&gt;&lt;L^@VLY#X^`I8Q\&lt;VUQ\&amp;H_W\[^-ZX&lt;^;XU]@0NR__P&amp;`SP`$`_2^Y._KF(O\".@I+&lt;-F';A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="send" Type="Folder">
		<Item Name="MSG_Prepare for Processing.vi" Type="VI" URL="../send/MSG_Prepare for Processing.vi"/>
		<Item Name="MSG_Process Remaining Data.vi" Type="VI" URL="../send/MSG_Process Remaining Data.vi"/>
		<Item Name="MSG_Start Processing.vi" Type="VI" URL="../send/MSG_Start Processing.vi"/>
		<Item Name="MSG_Stop Processing.vi" Type="VI" URL="../send/MSG_Stop Processing.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Calc FIT_CPERRC.vi" Type="VI" URL="../support/Calc FIT_CPERRC.vi"/>
		<Item Name="CPERRC_Function.vi" Type="VI" URL="../support/CPERRC_Function.vi"/>
		<Item Name="Fit Sweep.vi" Type="VI" URL="../support/Fit Sweep.vi"/>
		<Item Name="Get Guessed Fit Params.vi" Type="VI" URL="../support/Get Guessed Fit Params.vi"/>
		<Item Name="Get Next Electrode.vi" Type="VI" URL="../support/Get Next Electrode.vi"/>
		<Item Name="Open Fit TDMS.vi" Type="VI" URL="../support/Open Fit TDMS.vi"/>
		<Item Name="Process All.vi" Type="VI" URL="../support/Process All.vi"/>
		<Item Name="Write Fit TDMS.vi" Type="VI" URL="../support/Write Fit TDMS.vi"/>
	</Item>
	<Item Name="Test" Type="Folder">
		<Item Name="TEST - TDMS save.vi" Type="VI" URL="../Test/TEST - TDMS save.vi"/>
	</Item>
	<Item Name="Data Processing Actor.vi" Type="VI" URL="../Data Processing Actor.vi"/>
</Library>

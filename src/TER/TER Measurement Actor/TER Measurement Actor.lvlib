﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!**!!!*Q(C=\&gt;5`&lt;BJ"&amp;-@RH[-5&lt;LF"B'AM/&gt;7\!DF!(&amp;'EJ8C34U",3:-'6_]+3$\"HC$3OQ*8Y!K&lt;\YQ@/$3BC;UI9J&lt;"SW`_@:B&gt;VF)PN^*(D:@+\)_&amp;^L(87?`Z5NPJ?'TMJ`UV(J0[6&amp;V?]W.=EV&lt;$W@D:[?2MK8%];T^.-\PMPV"U,&gt;@S@Z@VR2`"G`]/&lt;N1?2.3E"N7JJL&lt;MG/2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2'XA^SE9N=Z*#3R:/&amp;EEG4#:,/5*3]*:\%EXA3$R_6?"*0YEE]C9=O3DS**`%EHM4$-#7?R*.Y%E`C9;IOS8[1YUE]4+`!%XA#4_!*0#SJQ"-!AM7#C9.*9#BI$%Y#4_!*0*QK]!3?Q".Y!A`.#DS"*`!%HM$$E,YLU48N)-@$.()]DM@R/"\(Q^2S0)\(]4A?R].S=DS/RU%Y#TK41Z!TS/HA@(!]DI=`=DS/R`%Y(M&gt;$5\^#XH?G;&gt;J"DM@Q'"\$9XA-$V0)]"A?QW.Y$!`4SP!9(M.D?!Q03]HQ'"\$9U#-26F?RG4'1+/4%2A?8PVOM8[6IEOM(V,&gt;P+K&lt;5H7TK7YCV=WBOOCKC[G[3+L.6WWK;L.5G[$[=CKU#K.;2$7Y&gt;&gt;3"^TVVI/[I'_K+OK$/K60KJ!X^SRU0BY0W_\W'9&gt;"ON^.GM^&amp;KN&gt;*CM&gt;"]0N&gt;U/N6E-DE^"DZRH"Y),]_F.?@$]P\(^NPDV[@F]]`NQ^W8JY@0XVP_$P_@`Y&amp;HIT\I^T89IV`&amp;]7!Y!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="send" Type="Folder">
		<Item Name="MSG_Measure.vi" Type="VI" URL="../send/MSG_Measure.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Calc Amplitude Correction.vi" Type="VI" URL="../support/Calc Amplitude Correction.vi"/>
		<Item Name="Calc Polar Coordinates.vi" Type="VI" URL="../support/Calc Polar Coordinates.vi"/>
		<Item Name="Calc Sweep Steps.vi" Type="VI" URL="../support/Calc Sweep Steps.vi"/>
		<Item Name="Correct Amplitude.vi" Type="VI" URL="../support/Correct Amplitude.vi"/>
		<Item Name="FGEN Frequency.vi" Type="VI" URL="../support/FGEN Frequency.vi"/>
		<Item Name="Get WFM Info.vi" Type="VI" URL="../support/Get WFM Info.vi"/>
		<Item Name="Meas Impedance Spectrum.vi" Type="VI" URL="../support/Meas Impedance Spectrum.vi"/>
		<Item Name="NI-FGEN_Setup.vi" Type="VI" URL="../support/NI-FGEN_Setup.vi"/>
		<Item Name="NI-SCOPE_Measure Amplitude.vi" Type="VI" URL="../support/NI-SCOPE_Measure Amplitude.vi"/>
		<Item Name="NI-SCOPE_Setup.vi" Type="VI" URL="../support/NI-SCOPE_Setup.vi"/>
		<Item Name="Open Spectrum TDMS.vi" Type="VI" URL="../support/Open Spectrum TDMS.vi"/>
		<Item Name="SCOPE Fetch WFM.vi" Type="VI" URL="../support/SCOPE Fetch WFM.vi"/>
		<Item Name="SCOPE Sample Rate.vi" Type="VI" URL="../support/SCOPE Sample Rate.vi"/>
		<Item Name="Switch Relay.vi" Type="VI" URL="../support/Switch Relay.vi"/>
		<Item Name="Write Spectrum TDMS.vi" Type="VI" URL="../support/Write Spectrum TDMS.vi"/>
	</Item>
	<Item Name="TER Measurement Actor.vi" Type="VI" URL="../TER Measurement Actor.vi"/>
</Library>

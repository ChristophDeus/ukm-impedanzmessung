﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)`!!!*Q(C=\&gt;5^DN.1&amp;-8R![+A^1Z1J&amp;H"X1'EIM]7\A;G3$NFNH$;E7CSAV'W="M7Y#WEIY;`HW^'I9")#"")9_=F^HF@P^D/CT3WN^)&lt;@&lt;WV[;@&lt;WG4M3^.R=HUQ+N@[^50@(83^OI&gt;''@78E[M_FSY`[._=K`H8_NP_8``[,^P,^N^P$T&gt;`"(`]&gt;`$K]N-NCCF*C85&amp;+0)C,`)C,`)C.\H*47ZSEZM]S:-]S:-]S:-]S)-]S)-]S//S.J',8/3+M8)5ER=4&amp;9-7!R3.I;BY+TS&amp;J`!5(EZ6?!J0Y3E]B9=G+DS&amp;J`!5HM*$.R7?QF.Y#E`B9;ABK&lt;'4YSE]$#`D-2\D-2\D95I:DQ'9S=T!:B!9-J8GQ(C-RXAYF0%9D`%9D`&amp;1,?-R(O-R(O/BS\AK(JJF*]@$-%I]C3@R**\%Q^"+0)EH]33?R-.U3DS**U%E%S;$1V$3+7G1H#3?R-/(%E`C34S**`&amp;1.?Z1DCOT;*;&gt;(%`A#4S"*`!%(I:1Y!E]A3@Q""['6?!*0)%H]!1?JF,A#4S"*Y!%ET+^AM'#DE'D)!A]P-&lt;4%O-O?5BC\/K`L`'AKB^!^9/F@G$5$Y,["KNPH0K'K#_U_A+K,YT[#[O`C"KIHFA^I,KBTLT0F"0F3$F1^J1&gt;:5P:5+;F[W^O?$[@.=_T4K?4DM?D$I?$^PO^&gt;LO&gt;NNON.JO.JGF[8A&lt;?M4]P#/O[^-$RF`OH_&gt;0(O_HR`M[0\`H]]02ZS@`#``-`M$&lt;KN;\HY"J^!V/$47U!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Cluster_Counter-Measurement.ctl" Type="VI" URL="../Cluster_Counter-Measurement.ctl"/>
	<Item Name="Cluster_Counter-Processing.ctl" Type="VI" URL="../Cluster_Counter-Processing.ctl"/>
	<Item Name="Cluster_Data Buffer.ctl" Type="VI" URL="../Cluster_Data Buffer.ctl"/>
	<Item Name="Cluster_Fit Coefficients.ctl" Type="VI" URL="../Cluster_Fit Coefficients.ctl"/>
	<Item Name="Cluster_FitParams.ctl" Type="VI" URL="../Cluster_FitParams.ctl"/>
	<Item Name="Cluster_FitResults.ctl" Type="VI" URL="../Cluster_FitResults.ctl"/>
	<Item Name="Cluster_Flags-Measurement.ctl" Type="VI" URL="../Cluster_Flags-Measurement.ctl"/>
	<Item Name="Cluster_Graph-Last Measurement.ctl" Type="VI" URL="../Cluster_Graph-Last Measurement.ctl"/>
	<Item Name="Cluster_Graph-TER History.ctl" Type="VI" URL="../Cluster_Graph-TER History.ctl"/>
	<Item Name="Cluster_MeasSetup.ctl" Type="VI" URL="../Cluster_MeasSetup.ctl"/>
	<Item Name="Cluster_Measurement Description.ctl" Type="VI" URL="../Cluster_Measurement Description.ctl"/>
	<Item Name="Cluster_Processed Sweep Info.ctl" Type="VI" URL="../Cluster_Processed Sweep Info.ctl"/>
	<Item Name="Cluster_PXI-Slots.ctl" Type="VI" URL="../Cluster_PXI-Slots.ctl"/>
	<Item Name="Cluster_Single Reading.ctl" Type="VI" URL="../Cluster_Single Reading.ctl"/>
	<Item Name="Cluster_Sweep Information.ctl" Type="VI" URL="../Cluster_Sweep Information.ctl"/>
	<Item Name="Cluster_SweepParams.ctl" Type="VI" URL="../Cluster_SweepParams.ctl"/>
	<Item Name="Cluster_TER History.ctl" Type="VI" URL="../Cluster_TER History.ctl"/>
	<Item Name="Enum_FGV_Measured-Channels.ctl" Type="VI" URL="../Enum_FGV_Measured-Channels.ctl"/>
	<Item Name="Enum_PXI-RelayType.ctl" Type="VI" URL="../Enum_PXI-RelayType.ctl"/>
	<Item Name="Enum_State.ctl" Type="VI" URL="../Enum_State.ctl"/>
</Library>

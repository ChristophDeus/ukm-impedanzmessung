﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16773906</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16776911</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)W!!!*Q(C=\&gt;8"&lt;2J"&amp;-&lt;RD]C2=E6*!29XHV],N)$3Q7O"K\H2QCMB7,\Z2AN@#\2!#ZP`T![7:&gt;HBEE22Z&amp;U?8L\:G@ERO[SFPHW2&lt;D2&gt;WT\`=LOU4X/VDV-`?.U_^&gt;@=/B_^[D]X4*?$.]?`^(^D`#PN\`OP&lt;0L90L&lt;`?\O`_C0YY\_$B&gt;K$C$*66&amp;+B.OVE=J/&lt;X/1G.XG2&amp;XG2&amp;XG2&amp;XG3*XG3*XG3*XG1"XG1"XG1"XH@S55O=J&amp;$-J/&lt;C=SA:A"T-B3:._-R(O-R(D\+?)T(?)T(?$B&amp;RG-]RG-]RE-X'9`R')`R'!^$&gt;9H\4I\(?"B?B;@Q&amp;*\#5XC95I7H!"34&amp;1-8A]"1U6A=&amp;*\#5XAY6/%J0)7H]"1?GF6Y#E`B+4S&amp;BSZ^6;JLWE[/BW'5?"*0YEE]C9?BF8A34_**0)G([:2Y%E_#3#:-"I?AJ&amp;.S1P)B]31?`CDR**\%EXA3$UX^#G6@G;:J/TG?Q".Y!E`A#4Q-I=!4?!*0Y!E]$+P!%XA#4_!*0%SFQ".Y!E]!#3:F?A7$"2W$EY)A]0$K&gt;UPUKV2&gt;%HW8RMWL=6.KX'Q;.Z('T;&amp;RU45OJM:&amp;UFB]D5867#S.2&gt;$Y=BJI$9T'*"K&gt;WYE[]X[CDN3"WF.&lt;;E/NK27V&lt;&amp;V`]YHH]VGHUUH(YV'(QU(\`6\&lt;\6;&lt;T5&lt;L^6KLV5L,Z@,Z-8$,`PR!G*^,^RTPPNYN&gt;E_0C^XXB]8OWY^?,@],`Z``A7?D0OHF(+T24W6?._]!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Send" Type="Folder">
		<Item Name="MSG_Back to MainMenu.vi" Type="VI" URL="../send/MSG_Back to MainMenu.vi"/>
	</Item>
	<Item Name="support" Type="Folder">
		<Item Name="Center Label and Caption on Pane.vi" Type="VI" URL="../support/Center Label and Caption on Pane.vi"/>
		<Item Name="Start Actor and Send Subpanel Reference.vi" Type="VI" URL="../support/Start Actor and Send Subpanel Reference.vi"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Item Name="Cluster_Config.ctl" Type="VI" URL="../typedefs/Cluster_Config.ctl"/>
		<Item Name="Cluster_Meas Hardware Settings.ctl" Type="VI" URL="../../Settings/Meas Active Settings/typedef/Cluster_Meas Hardware Settings.ctl"/>
		<Item Name="Cluster_ShowProperties.ctl" Type="VI" URL="../typedefs/Cluster_ShowProperties.ctl"/>
	</Item>
	<Item Name="Main UI.vi" Type="VI" URL="../Main UI.vi"/>
</Library>

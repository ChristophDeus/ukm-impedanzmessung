﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16776704</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">65282</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!*Q(C=\&gt;7^&lt;2N"%-8R2]-#H,)$AZFA+(IN-(!$T"R0#UQP:!P4!FNA#^-#7W!,Z`_N2A)&gt;3*@9BG&amp;I6UO2&lt;\^_WDO?J&amp;'_3*]VLZ7(&gt;]N&gt;`UQ&gt;,`?4@OW@8Q;^.8^?[6_&lt;`V\`'`[6II`S5@\P-KV_#@\Y^W#DZ5&amp;%+VL3AG9NW]Z&amp;8O2&amp;8O2&amp;8O2*HO2*HO2*HO2"(O2"(O2"(O1G.\H*47ZSEY^+,H+2CRR3M8GR5&lt;&amp;IM5!R')K+F]*4?!J0Y?'D#E`B+4S&amp;J`!Q2)7H]"3?QF.YG+&lt;#5XA+4_%J0#QV*$5K/:\#Q`*+0)EH]33?R-/73DQ*).EM74B:");3TO2.YEE]C9?X3DS**`%EHM2$NR*0YEE]C3@R-'7=3A\.5MHRM)Q#4_!*0)%H],#U!E`A#4S"*`#QH1*0Y!E1Q9&lt;"YB!54!I'""]#4_$BFQ*0Y!E]A3@QU$7O5)S4742,*=&gt;D0-:D0-:D0#QBYT%?YT%?YW&amp;:'9`R')`R'!^&lt;S8C-RXA-R'T+^D+,G9FGE!G-BZ^RNXB=J2Q3DSLVT;O_+&gt;5XG`IG5N]=[IOOPJDKC[1_@07BKA^,@1DK0U[.6G05G[AH,Q.VY`6+O^$/N"0N3$P1^L1&gt;&lt;&lt;N-`=U$&lt;\?&lt;LN?L,J?,TO?T4K?4DM?D$I?$^PO^&gt;LO&gt;NNPN[W0A+`8VA@$]8*JY0TX_W%R0XT@4.^JD.`+`]0`Z(XAW[J0O^_#-@A+TY3FO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Reply" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MSG_Init complete.vi" Type="VI" URL="../MSG_Init complete.vi"/>
		<Item Name="MSG_Release complete.vi" Type="VI" URL="../MSG_Release complete.vi"/>
	</Item>
	<Item Name="Send" Type="Folder">
		<Item Name="MSG_Init States.vi" Type="VI" URL="../MSG_Init States.vi"/>
		<Item Name="MSG_Release States.vi" Type="VI" URL="../MSG_Release States.vi"/>
	</Item>
	<Item Name="Actor_Init &amp; Release.vi" Type="VI" URL="../Actor_Init &amp; Release.vi"/>
</Library>

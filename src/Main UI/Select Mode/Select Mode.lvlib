﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12827549</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">15400704</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)^!!!*Q(C=\&gt;5`4O.1%-@R(SM+WNRAEZJOLJ!LZ!L45[6&amp;6%:#C(9E4J!LZ!)5=Y7U;#N@Q@N^D]%+4&gt;)!7KWQG=4Z_@XZZ.ER5N_OJ%N.Z\&lt;LEVM`XVLVKO0LNZLK@"^C_FBT0$?K:/ZQ&gt;,Y&gt;H_A`P&lt;_]6YV[V0_%`]SGH_VH_\_XW\-`AC``(6SI09CIJ)*SSN3GH:)]S:-]S:-]S9-]S)-]S)-]S*X=S:X=S:X=S9X=S)X=S)X=S0N/,H+2CRR3-HES54*I-E$3')K3F]34?"*0YO'D%E`C34S**`(12)EH]33?R*.Y[+&lt;%EXA34_**0!T6*&gt;FX=DS*B_%6?!*0Y!E]A9=J&amp;8A#1$":-(!Q#!Q&amp;*Y/$Q".Y!A_(#DS"*`!%HM$$;17?Q".Y!E`AI5N@F?C;NJ0D92AZ(M@D?"S0YW&amp;I/2\(YXA=D_.B/DE?R_-AH!G&gt;Q3()[?1U=$YY(M@$GRS0YX%]DM@R=+J@)?]LUT2N*]&gt;D?!S0Y4%]BI=B:(A-D_%R0);(976Y$)`B-4S'B[FE?!S0Y4%ARK2-,W-QI[02S!A-$X`^&lt;L&amp;_F;*,L/^3X&lt;SKGV*VM[FO)N8.I&lt;LIKIOJOECKR6=NKGKR6)OA_H)KN!KDGE46O485S/O"WF-\;K#WV):;5SNKU&lt;J_=M.R((5Y(,4@\\8&lt;\41-A\&lt;&lt;L4;&lt;D&gt;&lt;LN6;LF2;,R@Q9_-U_0R$?HEOX($`_?&lt;VZ'O[8$S]PSTP?H]@8:=O`Y@`T0`"MV#]&gt;T]%;`17RI&amp;]'!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Reply" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="MSG_Next State.vi" Type="VI" URL="../MSG_Next State.vi"/>
	</Item>
	<Item Name="Actor_Select Mode.vi" Type="VI" URL="../Actor_Select Mode.vi"/>
</Library>

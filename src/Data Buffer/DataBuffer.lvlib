﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16752888</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16759028</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!*Q(C=\&gt;7\41."%-&lt;R$U2!1/)/E&amp;O9&amp;NS!!R&gt;!-CUY*&lt;11%544QL8AF!B."]ANO)8DP]NQ-IE&gt;]""#\.\[]&gt;U_@OS&gt;$[G83_F#Y[FS&gt;&lt;4U]_WFNV90DOH]BTI&gt;YY@THRE`4BX@D`(Q`&amp;(`C;,`]F`_&gt;LE^_30Y^N`"G&gt;K$C*;UI$H.V*9&gt;ETT*ETT*ETT*ATT)ATT)ATT)H&gt;T*H&gt;T*H&gt;T*D&gt;T)D&gt;T)D&gt;T)?S58O=B&amp;$CF:0&amp;EIG43:)/E-2=F,YEE]C3@R]&amp;7**`%EHM34?/CCR*.Y%E`C34Q-5_**0)EH]31?JOK3\*5=4_*B?A7?Q".Y!E`A95E&amp;HA!1,":-(%Q#1](*Y%0A#4S"BY]+0)%H]!3?Q-.J":\!%XA#4_"B3._6[*J7S@%QD2S0YX%]DM@R-,5=D_.R0)\(]&lt;#=()`D=2$/AM\E%/1-=DIY8RS0Y_&amp;.DM@R/"\(YXAYV;_1^ZVJGF&lt;*]2A?QW.Y$)`B91I:(M.D?!S0Y7&amp;;'2\$9XA-D_&amp;B+2E?QW.Y$)CR+-P,G-Q9;(1S!M0$U?]7[V=JOM2[F?LG6&gt;W5KJN.&gt;2/J&lt;A\626&gt;&gt;4.6&amp;5GW_;F.6G[8;".5@JU+L-+J&amp;6).&lt;2_VZX&gt;'WN)'WI;VJ+^K#.K@.WN!P\LD@\\8&lt;\&lt;4&gt;&lt;D5-AT;&lt;D&gt;&lt;LN6;LF2;,B?&lt;TO7;TW@19O+:/$Y3XZ^)NH__@8W\OBI@F8&lt;X@0TUO7`Y$`Z^`Q&lt;.2ZTJ=ATV[";RL5X%!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Global DVR.vi" Type="VI" URL="../private/Global DVR.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Analyse Processing Progress.vi" Type="VI" URL="../public/Analyse Processing Progress.vi"/>
		<Item Name="Append New SweepReading.vi" Type="VI" URL="../public/Append New SweepReading.vi"/>
		<Item Name="Get All Sweep Data.vi" Type="VI" URL="../public/Get All Sweep Data.vi"/>
		<Item Name="Get Latest analyzed Sweep.vi" Type="VI" URL="../public/Get Latest analyzed Sweep.vi"/>
		<Item Name="Get Latest Sweep Data.vi" Type="VI" URL="../public/Get Latest Sweep Data.vi"/>
		<Item Name="Get Previous Fit Params.vi" Type="VI" URL="../public/Get Previous Fit Params.vi"/>
		<Item Name="Get specific Sweep Data.vi" Type="VI" URL="../public/Get specific Sweep Data.vi"/>
		<Item Name="Get TER Data.vi" Type="VI" URL="../public/Get TER Data.vi"/>
		<Item Name="Init Data Buffer.vi" Type="VI" URL="../public/Init Data Buffer.vi"/>
		<Item Name="Is last Sweep analyzed.vi" Type="VI" URL="../public/Is last Sweep analyzed.vi"/>
		<Item Name="Set Fit Results.vi" Type="VI" URL="../public/Set Fit Results.vi"/>
		<Item Name="Set TER Data.vi" Type="VI" URL="../public/Set TER Data.vi"/>
	</Item>
	<Item Name="Debug DVR.vi" Type="VI" URL="../Debug DVR.vi"/>
	<Item Name="Debug_Init DVR.vi" Type="VI" URL="../Debug_Init DVR.vi"/>
</Library>

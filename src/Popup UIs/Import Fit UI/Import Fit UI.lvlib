﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16752888</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16759028</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;5R&lt;NN!%)8BFS"&amp;#D;]1;!L4/6?"X#DTK5R/9*;FWI&amp;-!\G#LK#LD"6?FV"6W$_89]*Q9#N*AG-Q+37FBZX:T]N+6LKWV@JC_:LW`$GVM\01_P%M8@OS=8B[5R`6&gt;=[$00&amp;_0FZ`(R2[78^92B?'\_=(Z9CS`GX`6=W@7Q@W`_^06T^%@TVX]%HN1=2,7F"=ZKJ44MH?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:(XH6TE)B=ZJ'4S:++E;&amp;)A[1R&amp;S3(R**\%EXDYK-34?"*0YEE]&gt;&amp;(C34S**`%E(I9J]33?R*.Y%A_FOC4\4IYH]6"?A3@Q"*\!%XC95I%H!!34"97$)D!5H!T?"*\!%XBYK]!4?!*0Y!E]H&amp;&lt;A#4S"*`!%(I&lt;U69GO;4MZ(ML)]4A?R_.Y(!_FZ8A=D_.R0)[([?2Y()_$=#:UCE/1-]DJY(RQ0)[(0X)]DM@R/"\(Q[F_B&lt;SP4./UH2S0Y4%]BM@Q'"Z+S0!9(M.D?!Q0:76Y$)`B-4S'B[FE?!S0Y4%ARK2-,[/9-&gt;$I:!3'BV?`7[R@J?A3[\N5.[`KJF4&gt;&lt;+K&lt;3(6TK#[[[G+K,J*K]67,KFIMV3+IPJQ+L=+I*F%.&lt;BVVZHCC(7E(WI[WJ7VI;^K+.L;B@\DD_8T7[843]8D5Y8$1&lt;L@4&gt;LP6:L02?LX7;L83/)\,9_!&lt;_`*!?(IO0@"_OPN_]`BLWP_Y_TF/N^._OLX@N`Q@`(^_"]^'@&gt;&lt;F(+T2&lt;^]^4&lt;E!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="support" Type="Folder">
		<Item Name="Get Fit Data.vi" Type="VI" URL="../Get Fit Data.vi"/>
		<Item Name="Get Selected Fit Data.vi" Type="VI" URL="../Get Selected Fit Data.vi"/>
		<Item Name="TER Dataset Empty or Zero.vi" Type="VI" URL="../TER Dataset Empty or Zero.vi"/>
	</Item>
	<Item Name="TDMS" Type="Folder">
		<Item Name="Get Point Numbers.vi" Type="VI" URL="../TDMS/Get Point Numbers.vi"/>
		<Item Name="Get Rel.vi" Type="VI" URL="../TDMS/Get Rel.vi"/>
		<Item Name="Get Time.vi" Type="VI" URL="../TDMS/Get Time.vi"/>
	</Item>
	<Item Name="Import_FitData.vi" Type="VI" URL="../Import_FitData.vi"/>
</Library>

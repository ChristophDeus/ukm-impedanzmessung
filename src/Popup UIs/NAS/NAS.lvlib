﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16711680</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16755872</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!):!!!*Q(C=\&gt;4"&lt;3*"%)8BZZ50PJ,"#E+I&amp;%C"&amp;#I&amp;LHMEB5K"&amp;%DBJ5!+J.$_JSGRFI78SVKS,0@1'.ZU6X`O'5;;\56[VHD5.P^M@]_0[WP-=.QZ0T&lt;D/GB]-(]:-;?_7@2&gt;`?MCG]W^_L@TY_\]D`Q0GH\;4`P?\=`$(]'H`Q[?N$S)[+980?GB:&gt;FB=J/&lt;X/1G.XG2&amp;XG2&amp;XG2&amp;XG3*XG3*XG3*XG1"XG1"XG1"`E]S%5O=J&amp;$-IO&lt;B5R25]!-BC,T:DT'9TT'QV=:D`%9D`%9$U.E0-:D0-:D0%S4]2C0]2C0]6"K3DQ0=DT'1XE6HM*4?!J0Y7&amp;*&amp;:Y#5#R7&amp;#[+Q&amp;"RMPB1?!J0Y?'D#E`B+4S&amp;J`"Q7I7H]"3?QF.YG$*XJ;:G/=DR5%;**`%EHM34?#CNR*.Y%E`C34QMJ]34?"*%MG"3()+33=G!Z%PC34T]5?**0)EH]31?4MULF(.H&amp;MVSE/-*0)%H]!3?Q%-*":\!%XA#4_#BL!*0Y!E]A3@QM*1#4_!*0!%E7*4F&amp;21,*A;$AC$Q]*JX3]SL6&amp;-3]Z$[ZF8@F/K&lt;48U4K7]/^5688UTV26*PPHJ4V:OFXA4V0[&gt;'KT(K2&gt;34FY'[]([GH_B(_I'_J_`I7`K;PFKG`O?"F]N&amp;Z`.:J^.*R_.2B]."_`V?O^V/W_V7[`6;K^8K^BDYT8&amp;\)(S&amp;Z^+8-0T3WT89IV=,6VD_!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="typedefs" Type="Folder">
		<Item Name="Cluster_NAS-INI.ctl" Type="VI" URL="../../../Main UI/typedefs/Cluster_NAS-INI.ctl"/>
	</Item>
	<Item Name="NAS Connection.vi" Type="VI" URL="../NAS Connection.vi"/>
</Library>
